# Crear un nuevo espacio de trabajo en c9

Aquí se muestra como crear un nuevo espacio de trabajo en c9

## **Cloud9**

C9 es un entorno de desarrollo integrado en linea, soporta varios lenguajes de programacion como C,C++, PHP, Ruby, JavaScript, Node.js. En la actualidad es parte de Amazon Web Services.

## Tutorial 

* Ingresar a la pagina de cloud9 e ingresar las respectivas credenciales y dar clic en **sign in** 

**https://c9.io/login**

 ![](imagenes/login.png)

* Una vez dentro se cargaran los workspaces disponibles como se muesntra en la imagen.

* A continuacion se debera seleccionar "Crear un nuevo espacio de trabajo"

 ![](imagenes/espacio.png)


* Lo importante es ingresar el nombre del proyecto, lenguaje en el que esta realizado el proyecto y seleccionarlo publico.

 ![](imagenes/create.png)


* Una vez dentro del workspace del proyecto se debera clonar el proyecto, se utilizara comandos git.

``` 
git clone https://gitlab.com/proyecto/trabajo/proyectos/google/chat-bot-proyecto.git

```


* Entrar al directorio del proyecto y utilizar el siguiente comando para Instalar la version de node que desee, para la practica se usara 8.11.3.

```
nvm install 8.11.3
```
* Es importante que cada vez que desee correr su proyecto debe usar el siguiente comando para cambiar el la version de node que desee usar. 

```
nvm use 8.11.3
```

finalmente puede correr su programa con los comandos conocidos. 






<a href="https://twitter.com/alimonse29" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @alimonse29 </a><br>


